package client

import (
	"bitbucket.org/marco/secretshare/handlers"
	"bitbucket.org/marco/secretshare/types"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func GetSecret(action *types.ClientConfig) (secret string, err error) {
	url := action.ServerUrl.String() + handlers.SecretsAPI + action.SecretId
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var data types.DataResponseBody
	if err = json.Unmarshal(body, &data); err != nil {
		return "", err
	}
	return data.Data, nil
}
