package client

import (
	"bitbucket.org/marco/secretshare/handlers"
	"bitbucket.org/marco/secretshare/types"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func StoreSecret(action *types.ClientConfig) (secret string, err error) {
	url := action.ServerUrl.String() + handlers.SecretsAPI
	body, _ := json.Marshal(types.PostSecretRequestBody{PlainText: action.Secret})

	resp, err := http.Post(url, handlers.ContentJSON, bytes.NewBuffer(body))
	if err != nil {
		return "", err
	}

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var data types.IdResponseBody
	if err = json.Unmarshal(body, &data); err != nil {
		return "", err
	}
	return data.SecretId, nil
}
