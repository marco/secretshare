package main

import (
	"bitbucket.org/marco/secretshare/handlers"
	"flag"
	"fmt"
	"log"
	"net/http"
)

const (
	defaultPort = 8080
)

// main Manning LiveProject implementation.
// See https://liveproject.manning.com/module/292_2_2/build-a-secrets-sharing-web-application-in-go
func main() {
	var port = flag.Int("port", defaultPort, "Server port")
	flag.Parse()

	mux := handlers.CreateHandlers()
	log.Printf("Starting Secret Share Server on port %d", *port)
	host := fmt.Sprintf("0.0.0.0:%d", *port)
	http.ListenAndServe(host, mux)

}
