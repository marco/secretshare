package types

import "net/url"

type PostSecretRequestBody struct {
	PlainText string `json:"plain_text"`
}

type DataResponseBody struct {
	Data string `json:"data"`
}

type IdResponseBody struct {
	SecretId string `json:"id"`
}

type ClientConfig struct {
	Action string
	ServerUrl *url.URL
	Secret   string
	SecretId string
}
