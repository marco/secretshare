package handlers

import "net/http"

type HealthcheckHandler struct{}

func (p *HealthcheckHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Write([]byte("Server is UP"))
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}
