package handlers

import (
	"bitbucket.org/marco/secretshare/secrets"
	"bitbucket.org/marco/secretshare/types"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type SecretHandler struct{
	secrets secrets.SecretStore
}

func NewSecretHandler() *SecretHandler {
	p := new(SecretHandler)
	p.secrets = *secrets.NewSecretStore()
	return p
}

func (p *SecretHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		id, err := GetIdFromPath(r.URL.String())
		if err != nil {
			log.Printf("ERROR: %v", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		log.Printf("GET: /%s\n", id)
		w.Header().Set(ContentType, ContentJSON)
		secret := p.secrets.GetSecret(id)
		if secret == "" {
			log.Printf("ERROR: no secret found for ID[%s]", id)
			w.WriteHeader(http.StatusNotFound)
		}
		body, err := json.Marshal(types.DataResponseBody{Data: secret})
		if err == nil {
			w.Write(body)
		} else {
			log.Printf("ERROR: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
	} else if r.Method == "POST" {
		log.Printf("POST: secret")
		body, _ := ioutil.ReadAll(r.Body)

		if response, err := p.SaveSecret(body); err != nil {
			log.Printf("ERROR: %v", err)
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
		} else {
			w.Header().Set(ContentType, ContentJSON)
			// TODO: A 201 response should have a Location header with the corresponding URL.
			//		w.Header().Set("Location", fmt.Sprintf("/%s", id))
			w.WriteHeader(http.StatusCreated)
				w.Write(response)
		}
	}
}

func (p *SecretHandler) SaveSecret(body []byte) (response []byte, err error) {
	secret, err := GetTextFromBody(body)
	if err != nil {
		return
	}
	id := p.secrets.PutSecret(secret)
	response, err = json.Marshal(types.IdResponseBody{SecretId: id})
	return
}
