package handlers_test

import (
	"bitbucket.org/marco/secretshare/handlers"
	"bitbucket.org/marco/secretshare/types"
	"encoding/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
	"net/http/httptest"
	"strings"
)

var _ = Describe("Handlers", func() {

	Describe("When parsing path", func() {
		Context("if the path is empty", func() {
			path := "/"
			It("should return an error", func() {
				_, err := handlers.GetIdFromPath(path)
				Expect(err).Should(HaveOccurred())
			})
		})
		Context("if the path is multi-valued", func() {
			path := "/foo/bar"
			It("should return an error", func() {
				_, err := handlers.GetIdFromPath(path)
				Expect(err).Should(HaveOccurred())
			})
		})
		Context("if the path is non-empty", func() {
			path := "/12345"
			It("should return the ID", func() {
				id, err := handlers.GetIdFromPath(path)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(id).Should(Equal("12345"))
			})
		})
	})

	Describe("When hitting the health check", func() {
		Context("and the server is up", func() {
			mux := handlers.CreateHandlers()
			It("should return OK", func() {
				writer := httptest.NewRecorder()
				request, _ := http.NewRequest(http.MethodGet, handlers.HealthcheckAPI, nil)
				mux.ServeHTTP(writer, request)

				Ω(writer.Code).To(Equal(http.StatusOK))
				Ω(string(writer.Body.Bytes())).To(Equal("Server is UP"))
			})
		})
	})

	Describe("The SecretShare Server", func() {
		mux := handlers.CreateHandlers()
		Context("When storing a secret", func() {
			writer := httptest.NewRecorder()
			secret := types.PostSecretRequestBody{PlainText: "This is a secret test"}
			body, _ := json.Marshal(secret)

			request, _ := http.NewRequest(http.MethodPost, handlers.SecretsAPI,
				strings.NewReader(string(body)))

			var responseBody types.IdResponseBody
			var dataBody types.DataResponseBody

			It("should save it", func() {
				mux.ServeHTTP(writer, request)

				Ω(writer.Code).To(Equal(http.StatusCreated))
				Ω(json.Unmarshal(writer.Body.Bytes(), &responseBody)).Should(Succeed())
			})

			It("should return it when asked", func() {
				writer = httptest.NewRecorder()
				r, _ := http.NewRequest(http.MethodGet, handlers.SecretsAPI+responseBody.SecretId, nil)
				mux.ServeHTTP(writer, r)

				Ω(writer.Code).To(Equal(http.StatusOK))
				Ω(json.Unmarshal(writer.Body.Bytes(), &dataBody)).Should(Succeed())
				Ω(dataBody.Data).Should(Equal("This is a secret test"))
			})

			It("should return an empty response when asked again", func() {
				writer = httptest.NewRecorder()
				r, _ := http.NewRequest(http.MethodGet, handlers.SecretsAPI+responseBody.SecretId, nil)
				mux.ServeHTTP(writer, r)

				Ω(writer.Code).To(Equal(http.StatusNotFound))
				Ω(json.Unmarshal(writer.Body.Bytes(), &dataBody)).Should(Succeed())
				Ω(dataBody.Data).Should(Equal(""))
			})

			It("should return a 404 for an invalid ID", func() {
				writer = httptest.NewRecorder()
				r, _ := http.NewRequest(http.MethodGet, handlers.SecretsAPI+"foobar", nil)
				mux.ServeHTTP(writer, r)

				Ω(writer.Code).To(Equal(http.StatusNotFound))
			})

			It("should return a 400 for an invalid request", func() {
				writer = httptest.NewRecorder()
				r, _ := http.NewRequest(http.MethodGet, handlers.SecretsAPI, nil)
				mux.ServeHTTP(writer, r)

				Ω(writer.Code).To(Equal(http.StatusBadRequest))
			})

		})

		Context("When sending an empty secret", func() {
			writer := httptest.NewRecorder()
			secret := types.PostSecretRequestBody{PlainText: ""}
			body, _ := json.Marshal(secret)

			request, _ := http.NewRequest(http.MethodPost, handlers.SecretsAPI,
				strings.NewReader(string(body)))

			It("should refuse it", func() {
				mux.ServeHTTP(writer, request)
				Ω(writer.Code).To(Equal(http.StatusBadRequest))
			})
		})

		Context("When sending invalid JSON", func() {
			writer := httptest.NewRecorder()

			// Note the below is malformed JSON, not only not in the expected format ("plain_text").
			request, _ := http.NewRequest(http.MethodPost, handlers.SecretsAPI,
				strings.NewReader(`{"some": nonsense, "tuple": (3, 42, 666)}`))

			It("should refuse it", func() {
				mux.ServeHTTP(writer, request)
				Ω(writer.Code).To(Equal(http.StatusBadRequest))
			})
		})
	})
})
