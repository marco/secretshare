package handlers

import (
	"bitbucket.org/marco/secretshare/types"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

const (
	ContentType = "Content-Type"
	ContentJSON = "application/json"

	HealthcheckAPI = "/healthcheck"
	SecretsAPI     = "/"
)

// Exported just so that it can be tested.
func GetIdFromPath(path string) (string, error) {
	s := strings.Trim(path, "/")
	if len(s) == 0 {
		return s, fmt.Errorf("no ID in path: %s", path)
	}
	segments := strings.Split(s, "/")
	if len(segments) != 1 {
		return s, fmt.Errorf("invalid path: %s", path)
	}
	return segments[0], nil
}

func GetTextFromBody(body []byte) (string, error) {
	var jsonBody types.PostSecretRequestBody
	if err := json.Unmarshal(body, &jsonBody); err != nil {
		return "", err
	}
	if jsonBody.PlainText == "" {
		return "", fmt.Errorf("empty secret")
	}
	return jsonBody.PlainText, nil
}

func CreateHandlers() *http.ServeMux {
	hc := HealthcheckHandler{}
	sh := NewSecretHandler()

	mux := http.NewServeMux()
	mux.Handle(HealthcheckAPI, &hc)
	log.Println("Added health check endpoint:", HealthcheckAPI)
	mux.Handle(SecretsAPI, sh)
	log.Println("Added main endpoint for Secrets:", SecretsAPI)
	return mux
}
