package secrets_test

import (
	"bitbucket.org/marco/secretshare/secrets"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

)

var _ = Describe("Secrets Store", func() {
	Context("When created via NewSecretStore", func() {
		store := secrets.NewSecretStore()
		It("is not nil", func() {
			Ω(store).ShouldNot(BeNil())
			Ω(store.Secrets).ShouldNot(BeNil())
		})
		It("can store secrets", func() {
			secret := "test zekret dooo"
			hash := store.PutSecret(secret)
			Ω(store.Secrets[hash]).Should(Equal(secret))
		})
		It("gives back the secret, only once", func() {
			secret := "test FOR A zekret WORD"
			hash := store.PutSecret(secret)
			Ω(store.GetSecret(hash)).Should(Equal(secret))
			Ω(store.GetSecret(hash)).Should(Equal(""))
		})
	})
})
