package secrets

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"sync"
)

const (
	FilenameEnv  = "DATA_FILE_PATH"
	ServerUrlEnv = "SECRETS_HOST_URL"
)

type SecretStore struct {
	sync.Mutex
	Secrets         map[string]string
	secretsFilename string
}

func NewSecretStore() *SecretStore {
	p := new(SecretStore)
	p.Secrets = make(map[string]string)

	p.secretsFilename = os.Getenv(FilenameEnv)
	if p.secretsFilename == "" {
		log.Panicf("%s is not defined, cannot load secrets from file", FilenameEnv)
	}
	return p
}

func (p *SecretStore) PutSecret(plain string) (hash string) {
	hash = fmt.Sprintf("%x", md5.Sum([]byte(plain)))
	log.Printf("Hashing to `%s`", hash)

	p.Lock()
	defer p.Unlock()
	p.Secrets[hash] = plain
	p.saveSecrets()
	return
}

func (p *SecretStore) GetSecret(hash string) (plain string) {
	p.Lock()
	defer p.Unlock()

	plain = p.getSecretWithoutRemoving(hash)
	delete(p.Secrets, hash)
	p.saveSecrets()

	return
}

// getSecretWithoutRemoving retrieves the secret, without removing it from the store.
// Only used for tests.
func (p *SecretStore) getSecretWithoutRemoving(hash string) (plain string) {
	p.loadSecrets()
	plain = p.Secrets[hash]
	return
}

func (p *SecretStore) loadSecrets() {

	f, err := os.OpenFile(p.secretsFilename, os.O_RDONLY|os.O_CREATE, 0660)
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()

	filelen := p.getDataLen()
	buf := make([]byte, filelen)
	if _, err := io.ReadFull(f, buf); err != nil {
		if err == io.EOF {
			err = io.ErrUnexpectedEOF
		}
	}
	if len(buf) == 0 {
	 	// There are no secrets stored in the file, it was never created before.
		return
	}
	// We initialize a temporary map here from the file data,
	// as just reading the data directly into &p.secrets would
	// _merge_ the existing key/values with the data in the file.
	tmp := make(map[string]string)
	err = json.Unmarshal(buf, &tmp)
	if err != nil {
		log.Panic(err)
	}
	p.Secrets = tmp
}

func (p *SecretStore) getDataLen() (filelen int64) {
	info, err := os.Stat(p.secretsFilename)
	if err != nil {
		log.Panic(err)
	}
	if info.Mode().IsRegular() {
		filelen = info.Size()
	} else {
		log.Panic("File is not a regular file, not allowed")
	}
	return filelen
}

func (p *SecretStore) saveSecrets() {
	buf, err := json.Marshal(p.Secrets)
	if err != nil {
		log.Panic(err)
	}
	f, err := os.Create(p.secretsFilename)
	defer f.Close()
	if err != nil {
		log.Panic(err)
	}
	_, err = f.Write(buf)
	if err != nil {
		log.Panic(err)
	}
}