package secrets_test

import (
	"io/ioutil"
	"log"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestSecrets(t *testing.T) {
	RegisterFailHandler(Fail)
	log.SetOutput(ioutil.Discard)

	RunSpecs(t, "Secrets Suite")
}
