package main

import (
	"bitbucket.org/marco/secretshare/client"
	"bitbucket.org/marco/secretshare/secrets"
	"bitbucket.org/marco/secretshare/types"
	"flag"
	"fmt"
	"net/url"
	"os"
	"strings"
)

const (
	CreateAction = "create"
	ViewAction = "view"
)

func usage() {
	pathSegments := strings.Split(os.Args[0], "/")
	fmt.Fprintf(flag.CommandLine.Output(),
		`Usage: %s [--url URL] ACTION DATA

	Connects to a SecretShare server and executes ACTION, which is
	one of either 'create' or 'view'.

	If 'create' is specified then a string 'secret' must be provided and will be
	stored and the 'id' returned; otherwise, when 'view' is specified, the 'id'
	of the secret must be provided and will return the corresponding secret.`,
	pathSegments[len(pathSegments) - 1])
	fmt.Println()
	fmt.Println()

	flag.PrintDefaults()
}

func main() {
	var defaultHost = os.Getenv(secrets.ServerUrlEnv)
	helpStr := fmt.Sprintf("The URL for the Secret Share server; if not specified, it will use the %s " +
		"environment variable", secrets.ServerUrlEnv)
	var host = flag.String("url", defaultHost, helpStr)

	flag.Parse()
	if *host == "" {
		fmt.Println("ERROR: the Secrets Server URL must be specified")
		os.Exit(1)
	}

	flag.Usage = usage
	if flag.NArg() != 2 {
		flag.Usage()
		fmt.Println("ERROR: Expecting ACTION DATA arguments")
		os.Exit(1)
	}

	var action = flag.Arg(0)
	var data = flag.Arg(1)

	config := NewClientConfig(*host, action)
	res, err := SelectAction(config, data)(config)
	if err != nil {
		fmt.Printf("ERROR: %s\n", err)
		os.Exit(1)
	}
	fmt.Println(res)
}

func SelectAction(config *types.ClientConfig, data string) func(*types.ClientConfig) (string, error) {
	var doAction func(*types.ClientConfig) (string, error)
	switch config.Action {
	case CreateAction:
		config.Secret = data
		doAction = client.StoreSecret

	case ViewAction:
		config.SecretId = data
		doAction = client.GetSecret

	default:
		flag.Usage()
		fmt.Printf("ERROR: %s is not a valid action", config.Action)
		os.Exit(1)
	}
	return doAction
}

func NewClientConfig(host string, action string) *types.ClientConfig {
	var err error
	clientAction := types.ClientConfig{Action: action}
	clientAction.ServerUrl, err = url.Parse(host)
	if err != nil {
		flag.Usage()
		fmt.Printf("ERROR: %s is not a valid URL (%q)", host, err)
		os.Exit(1)
	}
	return &clientAction
}
